package cellular;

import java.util.Random;

import datastructure.CellGrid;
import datastructure.IGrid;

public class BriansBrain implements CellAutomaton {
	
	
	IGrid currentGeneration;

	public BriansBrain(int rows, int columns) {
		// TODO Auto-generated constructor stub
		currentGeneration = new CellGrid(rows, columns, CellState.DEAD);
		initializeCells();

	}

	@Override
	public CellState getCellState(int row, int column) {
		// TODO Auto-generated method stub
		return this.currentGeneration.get(row, column);
	}

	@Override
	public void initializeCells() {
		// TODO Auto-generated method stub
		Random random = new Random();
		for (int row = 0; row < currentGeneration.numRows(); row++) {
			for (int col = 0; col < currentGeneration.numColumns(); col++) {
				if (random.nextBoolean()) {
					currentGeneration.set(row, col, CellState.ALIVE);
				} else {
					currentGeneration.set(row, col, CellState.DEAD);
				}
			}
		}

	}

	@Override
	public void step() {
		// TODO Auto-generated method stub
		IGrid nextGeneration = currentGeneration.copy();

		for(int row=0; row<numberOfRows();row++) {
			for(int col=0 ; col <numberOfColumns(); col++) {
				CellState state = getNextCell(row,col);
				nextGeneration.set(row, col, state);
			}
		}

		currentGeneration = nextGeneration;

	}

	@Override
	public CellState getNextCell(int row, int col) {
		// TODO Auto-generated method stub
		boolean isAlive = getCellState(row, col) == CellState.ALIVE;		
		boolean isDead = getCellState(row, col) == CellState.DEAD;		
		boolean isDying = getCellState(row, col) == CellState.DYING;
		
		int livingNeighbours = countNeighbors(row,col,CellState.ALIVE);
		
		if(isAlive)
			return CellState.DYING;
		if(isDying)
			return CellState.DEAD;
		if(isDead && livingNeighbours==2)
			return CellState.ALIVE;
		//if(isAlive && livingNeighbours <2)
		//return CellState.DEAD;
		//if(isAlive && (livingNeighbours==2 || livingNeighbours==3))
		//	return CellState.ALIVE;
		//if(isAlive && livingNeighbours>3)
		//	return CellState.DEAD;
		//if(isDead && livingNeighbours==3)
		//	return CellState.ALIVE;
			
		return getCellState(row, col);
	}
	
	
	private int countNeighbors(int row, int col, CellState state) {
		// TODO
		int numNeighbours = 0;
		
		for(int dx=-1; dx<=1; dx++) {
			for(int dy=-1; dy<=1; dy++) {
				if(dx==0 && dy==0) {
					continue;
				}
				if(isValidLocation(row+dx,col+dy)) {
					CellState neighbour = getCellState(row+dx, col+dy);
					if(neighbour==state) {
						numNeighbours++;
					}
				}
			}
		}
		
		return numNeighbours;
	}

	
	private boolean isValidLocation(int row, int col) {
		if(row<0 || row>=numberOfRows())
			return false;
		if(col<0 || col>=numberOfColumns())
			return false;
		return true;
	}
	

	@Override
	public int numberOfRows() {
		// TODO Auto-generated method stub
		return this.currentGeneration.numRows();
	}

	@Override
	public int numberOfColumns() {
		// TODO Auto-generated method stub
		return this.currentGeneration.numColumns();
	}

	@Override
	public IGrid getGrid() {
		// TODO Auto-generated method stub
		return this.currentGeneration;
	}

}
