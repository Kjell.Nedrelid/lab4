package datastructure;

import java.util.ArrayList;

import cellular.CellState;

public class CellGrid implements IGrid {
	
	private int columns;
	private int rows;
	//private IGrid grid;
	private ArrayList<CellState> grid;
	

    public CellGrid(int rows, int columns, CellState initialState) {
		// TODO Auto-generated constructor stub
    	this.rows = rows;
    	this.columns = columns;
    	this.grid = new ArrayList<CellState>();
    	
    	
    	int numEntries = rows*columns;
		for(int i=0; i<numEntries; i++) {
			this.grid.add(initialState);
		}
	}

    @Override
    public int numRows() {
        // TODO Auto-generated method stub
        return this.rows;
    }

    @Override
    public int numColumns() {
        // TODO Auto-generated method stub
        return this.columns;
    }
    
	private int getIndex(int row, int col) {
		if(row >= this.numRows() || col>=this.numColumns()) {
			throw new IndexOutOfBoundsException("Too high index");
		}
		if(row < 0 || col<0) {
			throw new IndexOutOfBoundsException("Too low index");
		}
		return row*this.columns + col;
	}    

    @Override
    public void set(int row, int column, CellState element) {
        // TODO Auto-generated method stub
    	//this.grid[row][column] = element;
    	int index = getIndex(row, column);
        this.grid.set(index, element);
    }

    @Override
    public CellState get(int row, int column) {
        // TODO Auto-generated method stub
    	int index = getIndex(row, column);
        return this.grid.get(index);//this.grid[row][column].getColor();
    }

    @Override
    public IGrid copy() {
        // TODO Auto-generated method stub
		CellGrid newGrid = new CellGrid(this.rows, this.columns, CellState.DEAD);
		for(int row=0; row<this.rows; row++) {
			for(int col=0; col<this.columns;col++) {
				CellState value = this.get(row, col);
				newGrid.set(row, col, value);
			}
		}
		return newGrid;
    }
    
}
